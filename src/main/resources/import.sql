#import.sql file
INSERT INTO user_role (type) VALUES ('USER');
 
INSERT INTO user_role (type) VALUES ('ADMIN');
 
INSERT INTO user_role (type) VALUES ('DBA');

#users Admin
INSERT INTO users (id, username, password, state, imageUrl) VALUES ('1', 'admin', '$2a$10$O3gV3j6JVTF3i8wkqXRNVOETg4HT3hoaK3He2BG5LpgX1sMMhulGK', 'Active', 'http://www.jsweb.uk/images/loginascustomer_profile.jpg');
INSERT INTO user_user_roles (user_id, user_role_id) SELECT user.id, role.id FROM users user, user_role role where user.username='admin' and role.type='ADMIN';
INSERT INTO user_user_roles (user_id, user_role_id) SELECT user.id, role.id FROM users user, user_role role where user.username='admin' and role.type='USER';
INSERT INTO tokens (id, accessToken, refreshToken, validUntil, userId) values ('1', '9q7nJMKYkCcSMVyboElfSI37OACGMDvEXjOvtSFS6Dw=', '+cJPd1CjaBoVmOXJv6hwVE0bsEICjCel39xtrugE1iw=', '2018-04-30 10:46:44', '1');


#employees Dummy
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('1', 'design', 'mivic@email.com', 'Marko', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQb2LuXgKhPgQrPh41r2dMnDlY8jLgjByD0VU7Ef9-NCePj8l73Sw','Ivic','0922311232');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('2', 'design', 'manic@email.com', 'Marko', 'http://cdn2.hubspot.net/hub/63072/file-14997515-jpg/images/jeremywilcomb_thedanielgroup.jpg?t=1448922033049&width=229&height=229','Anic','0912543232');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('3', 'android', 'mhorvat@email.com', 'Matej', 'http://www.elitetranslingo.com/wp-content/uploads/2014/12/employee.png','Horvat','0982311232');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('4', 'android', 'aivanic@email.com', 'Ana', 'http://www.beyondcareersuccess.com/wp-content/uploads/2012/08/employee.png','Ivanic','0982311232');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('5', 'iOS', 'vvjekovic@email.com', 'Vjeka', 'http://drstianalytics.com/wp-content/uploads/2014/01/awc-welcome-girl.png','Vjekovic','0992318765');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('6', 'iOS', 'akovacevic@email.com', 'Anita', 'http://pubpages.unh.edu/~cfu27/employee%20number%20one.jpg','Kovacevic','0992311098');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('7', 'managment', 'ddanic@email.com', 'Dejan', 'https://pbs.twimg.com/profile_images/433026535738470400/KcGjEiKX.jpeg','Danic','0952981232');
INSERT INTO employees (id, division, email, firstName, imageUrl, lastName, phone) VALUES ('8', 'managment', 'mmaric@email.com', 'Marko', 'http://www.bhsf.co.uk/images/home/corporate.png','Maric','0952311762');

#rooms Dummy
INSERT INTO rooms (id, description, name, size) VALUES ('1', 'Large conference room', 'Large conference room', '20');
INSERT INTO rooms (id, description, name, size) VALUES ('2', 'Small conference room', 'Small conference room', '10');
INSERT INTO rooms (id, description, name, size) VALUES ('3', 'Meeting room', 'Meeting room', '8');
