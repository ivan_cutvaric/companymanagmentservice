package server.api.model;

import java.util.List;

public class RoomApi {

	private Long id;
	private String name;
	private String description;
	private int size;
	private List<RoomReservationApi> reservations;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public List<RoomReservationApi> getReservations() {
		return reservations;
	}
	public void setReservations(List<RoomReservationApi> reservations) {
		this.reservations = reservations;
	}

}
