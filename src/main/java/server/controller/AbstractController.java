package server.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.util.UriComponentsBuilder;

import server.dao.Entity;
import server.service.Service;

public abstract class AbstractController<T extends Entity<Long>, A> {

	protected abstract Service<T, A> getService(); 
	
	protected abstract String getBasePath();
	
	
	public ResponseEntity<List<A>> listAll() {
        List<T> entities = getService().getAll();
        if(entities.isEmpty()){
            return new ResponseEntity<List<A>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<A>>(getService().convertToApi(entities), HttpStatus.OK);
    }
	
    public ResponseEntity<A> get(long id) {
        T entity = getService().get(id);
        if (entity == null) {
            return new ResponseEntity<A>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<A>(getService().convertToApi(entity), HttpStatus.OK);
    }
 
    public ResponseEntity<Void> create(A entityApi, UriComponentsBuilder ucBuilder) {
        T entity = getService().convertFromApi(entityApi);
    	
        getService().create(entity);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path(getBasePath() + "/{id}").buildAndExpand(entity.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    public ResponseEntity<A> update(long id, A entityApi) {
    
    	T currentEntity = getService().get(id);
         
        if (currentEntity==null) {
            return new ResponseEntity<A>(HttpStatus.NOT_FOUND);
        }
        
        T entity = getService().convertFromApi(entityApi);
 
        getService().update(entity);
      
        return new ResponseEntity<A>(getService().convertToApi(entity), HttpStatus.OK);
    }
 
    public ResponseEntity<A> delete(long id) {
        T entity = getService().get(id);
        if (entity == null) {
            return new ResponseEntity<A>(HttpStatus.NOT_FOUND);
        }
        
        getService().delete(id);
        return new ResponseEntity<A>(HttpStatus.NO_CONTENT);
    }
    
    protected String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
	
}
