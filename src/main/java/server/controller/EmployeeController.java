package server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import server.api.model.EmployeeApi;
import server.model.Employee;
import server.service.EmployeeService;
import server.service.Service;

@RestController
public class EmployeeController extends AbstractController<Employee, EmployeeApi> {

	@Autowired
	private EmployeeService employeeService;
	
	@Override
	protected Service<Employee, EmployeeApi> getService() {
		return employeeService;
	}

	@Override
	protected String getBasePath() {
		return "/employees";
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/employees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeApi>> listAllEmployees() {
        return listAll();
    }
	
	@PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/employees/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeApi> getEmployee(@PathVariable("id") long id) {
        return get(id);
    }
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/employees", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createEmployee(@RequestBody EmployeeApi employeeApi, UriComponentsBuilder ucBuilder) {
        return create(employeeApi, ucBuilder);
    }
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeApi> updateEmployee(@PathVariable("id") long id, @RequestBody EmployeeApi employeeApi) {
        return update(id, employeeApi);
    }

	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeApi> deleteEmployee(@PathVariable("id") long id) {
        return delete(id);
    }
	
}
