package server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import server.api.model.EventApi;
import server.model.Event;
import server.service.EventService;
import server.service.Service;

@RestController
public class EventController extends AbstractController<Event, EventApi> {

	@Autowired
	private EventService eventService;
	
	@Override
	protected Service<Event, EventApi> getService() {
		return eventService;
	}

	@Override
	protected String getBasePath() {
		return "/events";
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventApi>> listAllEvents() {
        return listAll();
    }
	
	@PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/events/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventApi> getEvent(@PathVariable("id") long id) {
        return get(id);
    }
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/events", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createEvent(@RequestBody EventApi eventApi, UriComponentsBuilder ucBuilder) {
        return create(eventApi, ucBuilder);
    }

	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/events/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventApi> updateEvent(@PathVariable("id") long id, @RequestBody EventApi eventApi) {
        return update(id, eventApi);
    }

	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/events/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventApi> deleteEvent(@PathVariable("id") long id) {
        return delete(id);
    }
	
	
}
