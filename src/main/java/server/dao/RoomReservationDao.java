package server.dao;

import java.util.List;

import server.model.RoomReservation;

public interface RoomReservationDao extends Dao<Long, RoomReservation> {

	List<RoomReservation> readByRoom(Long roomId);
	
}
