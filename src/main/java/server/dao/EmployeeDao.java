package server.dao;

import server.model.Employee;

public interface EmployeeDao extends Dao<Long, Employee> {

}
