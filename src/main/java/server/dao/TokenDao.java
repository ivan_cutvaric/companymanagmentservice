package server.dao;

import server.model.Token;

public interface TokenDao extends Dao<Long, Token> {

	Token readByToken(String token);
	
}
