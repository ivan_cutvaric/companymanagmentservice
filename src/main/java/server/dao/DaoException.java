package server.dao;

public class DaoException extends RuntimeException {

	private static final long serialVersionUID = -1127337231833113599L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}
	
}
