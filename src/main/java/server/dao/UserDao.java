package server.dao;

import server.model.User;

public interface UserDao extends Dao<Long, User> {

	User readByUsername(String username);
	
}
