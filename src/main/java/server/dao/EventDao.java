package server.dao;

import java.util.Date;
import java.util.List;

import server.model.Event;

public interface EventDao extends Dao<Long, Event> {

	List<Event> readFromInterval(Date from, Date to);
	
}
