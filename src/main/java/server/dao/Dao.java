package server.dao;

import java.io.Serializable;
import java.util.List;

public interface Dao<PK extends Serializable, T>{

	T readById(PK id);
 
    PK create(T entity);
    
    void update(T entity);
 
    void delete(T entity);
    
    void deleteById(PK id);
    
	List<T> readAll();
    
    List<T> readAllByCriteria(String property, Object value);
    
	List<T> readAllByCriteria(String[] properties, Object[] values);
    
    T readByCriteria(String property, Object value);
    
	T readByCriteria(String[] properties, Object[] values);
	
	List<T> readAllBySql(String sql, Object[] parameters);
    
    boolean exists(T entity);
    
    boolean exists(PK id);
	
}
