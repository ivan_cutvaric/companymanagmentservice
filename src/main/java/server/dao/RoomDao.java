package server.dao;

import server.model.Room;

public interface RoomDao extends Dao<Long, Room> {

}
