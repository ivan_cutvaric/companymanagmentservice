package server.dao.jpa;

import server.model.Room;

import org.springframework.stereotype.Repository;

import server.dao.RoomDao;

@Repository("roomDao")
public class RoomJpaDao extends JpaDao<Long, Room> implements RoomDao {

	public RoomJpaDao() {
		super(Room.class);
	}

}
