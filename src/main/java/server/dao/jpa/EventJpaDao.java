package server.dao.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import server.model.Event;
import server.dao.EventDao;

@Repository("eventDao")
public class EventJpaDao extends JpaDao<Long, Event> implements EventDao {
	
	public EventJpaDao() {
		super(Event.class);
	}

	@Override
	public List<Event> readFromInterval(Date from, Date to) {
		return readAllBySql(
				"select e from Event e WHERE e.startDateTime BETWEEN ?0 AND ?1",
				new Date[] {from, to});
	}
}
