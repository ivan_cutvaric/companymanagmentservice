package server.dao.jpa;

import server.model.User;

import org.springframework.stereotype.Repository;

import server.dao.UserDao;

@Repository("userDao")
public class UserJpaDao extends JpaDao<Long, User> implements UserDao {

	private static final String PROPERTY_USERNAME = "username";
	
	public UserJpaDao() {
		super(User.class);
	}

	@Override
	public User readByUsername(String username) {
		return readByCriteria(PROPERTY_USERNAME, username);
	}
	
	

}
