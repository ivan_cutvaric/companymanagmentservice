package server.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import server.model.RoomReservation;
import server.dao.RoomReservationDao;

@Repository("roomReservationDao")
public class RoomReservationJpaDao extends JpaDao<Long, RoomReservation> implements RoomReservationDao {

	private static final String PROPERTY_ROOM = "room";
	
	public RoomReservationJpaDao() {
		super(RoomReservation.class);
	}

	@Override
	public List<RoomReservation> readByRoom(Long roomId) {
		return readAllByCriteria(PROPERTY_ROOM, roomId);
	}
	
	

}
