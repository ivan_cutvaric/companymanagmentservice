package server.dao.jpa;
 
import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import server.dao.Dao;
import server.dao.Entity;

public abstract class JpaDao<PK extends Serializable, T extends Entity<PK>> implements Dao<PK, T> {
     
    private final Class<T> persistentClass;
     
    public JpaDao(Class<T> persistenClass){
        this.persistentClass = persistenClass;
    }
    
    @Autowired
    private SessionFactory sessionFactory;
 
    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }
 
    @SuppressWarnings("unchecked")
    public T readById(PK id) {
        return (T) getSession().get(persistentClass, id);
    }
 
    public PK create(T entity) {
        getSession().persist(entity);
        return entity.getId();
    }
    
    public void update(T entity) {
    	getSession().merge(entity);
    }
 
    public void delete(T entity) {
        getSession().delete(entity);
    }
    
    public void deleteById(PK id) {
    	T entity = readById(id);
    	delete(entity);
    }
    
    @SuppressWarnings("unchecked")
	public List<T> readAll() {
    	Criteria criteria = createEntityCriteria();
        return (List<T>) criteria.list();
    }
    
    public List<T> readAllByCriteria(String property, Object value) {
    	return readAllByCriteria(new String[] {property}, new Object[] {value});
    }
    
    @SuppressWarnings("unchecked")
	public List<T> readAllByCriteria(String[] properties, Object[] values) {
    	Criteria criteria = createEntityCriteria();
    	
    	for (int i=0; i<properties.length; i++) {
    		criteria.add(Restrictions.eq(properties[i], values[i]));
    	}
    	
    	return criteria.list();
    }
    
    public T readByCriteria(String property, Object value) {
    	return readByCriteria(new String[] {property}, new Object[] {value});
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<T> readAllBySql(String sql, Object[] parameters) {
    	SQLQuery sqlQuery = getSession().createSQLQuery(sql);
    	for (int i=0; i<parameters.length; i++) {
    		sqlQuery.setParameter(i, parameters[i]);
    	}
    	return sqlQuery.list();
    }
    
    @SuppressWarnings("unchecked")
	public T readByCriteria(String[] properties, Object[] values) {
    	Criteria criteria = createEntityCriteria();
    	
    	for (int i=0; i<properties.length; i++) {
    		criteria.add(Restrictions.eq(properties[i], values[i]));
    	}
    	
    	return (T) criteria.uniqueResult();
    }
    
    public boolean exists(T entity) {
    	return exists(entity.getId());
    }
    
    @Override
    public boolean exists(PK id) {
    	return readById(id) != null;
    }
     
    protected Criteria createEntityCriteria(){
        return getSession().createCriteria(persistentClass);
    }
 
}