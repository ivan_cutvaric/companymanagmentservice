package server.dao.jpa;

import org.springframework.stereotype.Repository;

import server.model.Token;
import server.dao.TokenDao;

@Repository("tokenDao")
public class TokenJpaDao extends JpaDao<Long, Token> implements TokenDao {

	private static final String PROPERTY_ACCESS_TOKEN = "accessToken";
	
	public TokenJpaDao() {
		super(Token.class);
	}
	
	public Token readByToken(String token) {
		return readByCriteria(PROPERTY_ACCESS_TOKEN, token);
	}

}
