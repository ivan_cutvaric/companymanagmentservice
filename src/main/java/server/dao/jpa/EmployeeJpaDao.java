package server.dao.jpa;

import server.model.Employee;

import org.springframework.stereotype.Repository;

import server.dao.EmployeeDao;

@Repository("employeeDao")
public class EmployeeJpaDao extends JpaDao<Long, Employee>implements EmployeeDao {

	public EmployeeJpaDao() {
		super(Employee.class);
	}

}
