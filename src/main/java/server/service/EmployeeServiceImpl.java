package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.api.model.EmployeeApi;
import server.model.Employee;
import server.dao.EmployeeDao;

@Service("employeeService")
public class EmployeeServiceImpl extends AbstractService<Employee, EmployeeApi> implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	public Employee convertFromApi(EmployeeApi employeeApi) {
		Employee employee = new Employee();
		employee.setId(employeeApi.getId());
		employee.setFirstName(employeeApi.getFirstName());
		employee.setLastName(employeeApi.getLastName());
		employee.setPhone(employeeApi.getPhone());
		employee.setEmail(employeeApi.getEmail());
		employee.setDivision(employeeApi.getDivision());
		employee.setImageUrl(employeeApi.getImageUrl());
		return employee;
	}
	
	public EmployeeApi convertToApi(Employee employee) {
		EmployeeApi employeeApi = new EmployeeApi();
		employeeApi.setId(employee.getId());
		employeeApi.setFirstName(employee.getFirstName());
		employeeApi.setLastName(employee.getLastName());
		employeeApi.setPhone(employee.getPhone());
		employeeApi.setEmail(employee.getEmail());
		employeeApi.setDivision(employee.getDivision());
		employeeApi.setImageUrl(employee.getImageUrl());
		return employeeApi;
	}

	@Override
	protected EmployeeDao getDao() {
		return employeeDao;
	}
	
}
