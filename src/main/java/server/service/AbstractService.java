package server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import server.dao.Dao;

@Transactional
public abstract class AbstractService<T, A> implements Service<T, A> {

	protected abstract Dao<Long, T> getDao();
	
	public Long create(T entity) {
		return getDao().create(entity);
	}
	
	public T get(long id) {
		return getDao().readById(id);
	}
	
	public List<T> getAll() {
		return getDao().readAll();
	}

	public void update(T entity) {
		getDao().update(entity);
	}
	
	public void delete(T entity) {
		getDao().delete(entity);
	}
	
	public void delete(long id) {
		getDao().deleteById(id);
	}
	
	public boolean exists(T entity) {
		return getDao().exists(entity);
	}
	
	public List<T> convertFromApi(List<A> entitiesApi) {
		List<T> entities = new ArrayList<T>();
		for (A entityApi : entitiesApi) {
			entities.add(convertFromApi(entityApi));
		}
		return entities;
	}
	
	public List<A> convertToApi(List<T> entities) {
		List<A> entitiesApi = new ArrayList<A>();
		for (T entity : entities) {
			entitiesApi.add(convertToApi(entity));
		}
		return entitiesApi;
	}
	
}
