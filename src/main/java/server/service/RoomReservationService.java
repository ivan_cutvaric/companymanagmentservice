package server.service;

import java.util.List;

import server.api.model.RoomReservationApi;
import server.model.RoomReservation;

public interface RoomReservationService extends Service<RoomReservation, RoomReservationApi> {
	
	List<RoomReservation> getByRoom(Long roomId);
	
}
