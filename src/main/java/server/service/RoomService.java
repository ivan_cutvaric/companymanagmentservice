package server.service;

import server.api.model.RoomApi;
import server.model.Room;

public interface RoomService extends Service<Room, RoomApi> {
	
}
