package server.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.api.model.EventApi;
import server.dao.EventDao;
import server.model.Event;

@Service("eventService")
public class EventServiceImpl extends AbstractService<Event, EventApi> implements EventService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EventDao eventDao;
	
	public Event convertFromApi(EventApi eventApi) {
		Event event = new Event();
		event.setId(eventApi.getId());
		event.setTitle(eventApi.getTitle());
		event.setStartDateTime(eventApi.getStartDateTime());
		event.setEndDateTime(eventApi.getEndDateTime());
		event.setDescription(eventApi.getDescription());
		event.setImageUrl(eventApi.getImageUrl());
		event.setCreator(userService.convertFromApi(eventApi.getCreator()));
		return event;
	}
	
	public EventApi convertToApi(Event event) {
		EventApi eventApi = new EventApi();
		eventApi.setId(event.getId());
		eventApi.setTitle(event.getTitle());
		eventApi.setStartDateTime(event.getStartDateTime());
		eventApi.setEndDateTime(event.getEndDateTime());
		eventApi.setDescription(event.getDescription());
		eventApi.setImageUrl(event.getImageUrl());
		eventApi.setCreator(userService.convertToApi(event.getCreator()));
		return eventApi;
	}
	
	public List<Event> getFromInterval(Date from, Date to) {
		return getDao().readFromInterval(from, to);
	}

	@Override
	protected EventDao getDao() {
		return eventDao;
	}
	
}
