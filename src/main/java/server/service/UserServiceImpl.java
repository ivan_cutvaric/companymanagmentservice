package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import server.api.model.UserApi;
import server.dao.UserDao;
import server.model.State;
import server.model.User;

@Service("userService")
public class UserServiceImpl extends AbstractService<User, UserApi> implements UserService {

	@Autowired
	private UserDao userDao;
	
	public User convertFromApi(UserApi userApi) {
		User user = new User();
		user.setId(userApi.getId());
		user.setUsername(userApi.getUsername());
		user.setPassword(encryptPassword(userApi.getPassword()));
		user.setImageUrl(userApi.getImageUrl());
		return user;
	}
	
	public UserApi convertToApi(User user) {
		UserApi userApi = new UserApi();
		userApi.setId(user.getId());
		userApi.setUsername(user.getUsername());
		userApi.setPassword(user.getPassword());
		userApi.setImageUrl(user.getImageUrl());
		return userApi;
	}
	
	public User getByUsername(String username) {
		return getDao().readByUsername(username);
	}

	@Override
	protected UserDao getDao() {
		return userDao;
	}

	@Override
	public boolean isEnabled(User user) {
		return user.getState().equals(State.ACTIVE.getState());
	}

	@Override
	public boolean isNonExpired(User user) {
		return true;
	}

	@Override
	public boolean areCredentialsNonExpired(User user) {
		return true;
	}

	@Override
	public boolean isNonLocked(User user) {
		return !user.getState().equals(State.LOCKED.getState());
	}
	
	private String encryptPassword(String password) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}
	
}
