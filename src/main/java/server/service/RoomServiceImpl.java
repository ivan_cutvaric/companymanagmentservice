package server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.api.model.RoomApi;
import server.api.model.RoomReservationApi;
import server.model.Room;
import server.dao.RoomDao;

@Service("roomService")
public class RoomServiceImpl extends AbstractService<Room, RoomApi> implements RoomService {
	
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private RoomReservationService roomReservationService;
	
	public Room convertFromApi(RoomApi roomApi) {
		Room room = new Room();
		room.setId(roomApi.getId());
		room.setName(roomApi.getName());
		room.setDescription(roomApi.getDescription());
		room.setSize(roomApi.getSize());
		return room;
	}
	
	public RoomApi convertToApi(Room room) {
		RoomApi roomApi = new RoomApi();
		roomApi.setId(room.getId());
		roomApi.setName(room.getName());
		roomApi.setDescription(room.getDescription());
		roomApi.setSize(room.getSize());
		List<RoomReservationApi> reservations = roomReservationService.convertToApi(roomReservationService.getByRoom(room.getId()));
		roomApi.setReservations(reservations);
		return roomApi;
	}

	@Override
	protected RoomDao getDao() {
		return roomDao;
	}
	
}
