package server.service;

import java.util.List;

public interface Service<T, A> {

	Long create(T entity);
	
	T get(long id);
	
	List<T> getAll();

	void update(T entity);
	
	void delete(T entity);
	
	void delete(long id);
	
	boolean exists(T entity);
	
	A convertToApi(T entity);
	
	T convertFromApi(A entityApi);
	
	List<A> convertToApi(List<T> entities);
	
	List<T> convertFromApi(List<A> entitiesApi);
	
}
