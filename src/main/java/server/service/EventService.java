package server.service;

import java.util.Date;
import java.util.List;

import server.api.model.EventApi;
import server.model.Event;

public interface EventService extends Service<Event, EventApi> {
	
	List<Event> getFromInterval(Date from, Date to);
	
}
