package server.service;

import server.api.model.EmployeeApi;
import server.model.Employee;

public interface EmployeeService extends Service<Employee, EmployeeApi> {
	
}
