package server.service;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;

import server.dao.UserDao;
import server.api.model.TokenApi;
import server.dao.Dao;
import server.dao.TokenDao;
import server.model.User;
import server.model.Token;

@Service("tokenService")
public class TokenServiceImpl extends AbstractService<Token, TokenApi> implements TokenService {
	
	@Autowired
	private TokenDao tokenDao;
	
	@Autowired
	private UserDao accountDao;
	
	public TokenApi convertToApi(Token entity) {
		TokenApi tokenApi = new TokenApi();
		tokenApi.setAccessToken(entity.getAccessToken());
		tokenApi.setRefreshToken(entity.getRefreshToken());
		Date now = new Date();
		long expiresIn = entity.getValidUntil().getTime() - now.getTime();
		tokenApi.setExpiresIn(expiresIn);
		tokenApi.setTokenType("Bearer");
		return tokenApi;
	}

	public Token convertFromApi(TokenApi entityApi) {
		Token token = new Token();
		token.setAccessToken(entityApi.getAccessToken());
		token.setRefreshToken(entityApi.getRefreshToken());
		return token;
	}

	@Override
	protected Dao<Long, Token> getDao() {
		return tokenDao;
	}
	
	public Token create(UserDetails userDetails) {
		User account = accountDao.readByUsername(userDetails.getUsername());
		Token token = new Token();
		token.setAccessToken(generateToken());
		token.setUser(account);
		token.setRefreshToken(generateToken());
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, 7);
		token.setValidUntil(now.getTime());
		tokenDao.create(token);
		return token;
	}
	
	public Token getByToken(String token) {
		return tokenDao.readByToken(token);
	}
	
	public User deleteByToken(String token) {
		Token entity = getByToken(token);
		User account = entity.getUser();
		delete(entity);
		return account;
	}
	
	private String generateToken() {
		byte[] tokenBytes = new byte[32];
		new SecureRandom().nextBytes(tokenBytes);
		return new String(Base64.encode(tokenBytes), StandardCharsets.UTF_8);
	}
	
}
