package server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.api.model.RoomReservationApi;
import server.model.RoomReservation;
import server.dao.RoomReservationDao;

@Service("roomReservationService")
public class RoomReservationServiceImpl extends AbstractService<RoomReservation, RoomReservationApi> implements RoomReservationService {
	
	@Autowired
	private RoomReservationDao roomReservationDao;
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private UserService userService;
	
	public RoomReservation convertFromApi(RoomReservationApi roomReservationApi) {
		RoomReservation roomReservation = new RoomReservation();
		roomReservation.setId(roomReservationApi.getId());
		roomReservation.setRoom(roomService.get(roomReservationApi.getRoomId()));
		roomReservation.setUser(userService.convertFromApi(roomReservationApi.getUserApi()));
		roomReservation.setStartDateTime(roomReservationApi.getStartDateTime());
		roomReservation.setEndDateTime(roomReservationApi.getEndDateTime());
		roomReservation.setDescription(roomReservationApi.getDescription());
		return roomReservation;
	}
	
	public RoomReservationApi convertToApi(RoomReservation roomReservation) {
		RoomReservationApi roomReservationApi = new RoomReservationApi();
		roomReservationApi.setId(roomReservation.getId());
		roomReservationApi.setRoomId(roomReservation.getRoom().getId());
		roomReservationApi.setUserApi(userService.convertToApi(roomReservation.getUser()));
		roomReservationApi.setStartDateTime(roomReservation.getStartDateTime());
		roomReservationApi.setEndDateTime(roomReservation.getEndDateTime());
		roomReservationApi.setDescription(roomReservation.getDescription());
		return roomReservationApi;
	}
	
	public List<RoomReservation> getByRoom(Long roomId) {
		List<RoomReservation> roomReservations = roomId == null ? getDao().readAll() : getDao().readByRoom(roomId);
		return roomReservations;
	}

	@Override
	protected RoomReservationDao getDao() {
		return roomReservationDao;
	}
	
}
