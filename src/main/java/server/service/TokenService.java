package server.service;

import org.springframework.security.core.userdetails.UserDetails;

import server.model.User;
import server.api.model.TokenApi;
import server.model.Token;

public interface TokenService extends Service<Token, TokenApi> {

	Token create(UserDetails userDetails);
	
	Token getByToken(String token);
	
	User deleteByToken(String token);
	
}
