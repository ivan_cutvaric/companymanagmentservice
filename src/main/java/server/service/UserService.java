package server.service;

import server.model.User;
import server.api.model.UserApi;

public interface UserService extends Service<User, UserApi> {
	
	User getByUsername(String username);
	
	boolean isEnabled(User user);

	boolean isNonExpired(User user);

	boolean areCredentialsNonExpired(User user);

	boolean isNonLocked(User user);
	
}
