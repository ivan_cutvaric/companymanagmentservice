package server.security;

import org.springframework.security.core.userdetails.UserDetails;

import server.model.Token;
import server.model.User;

public interface TokenManager {

	Token createNewToken(UserDetails userDetails);

	User removeToken(String token);

	User getUser(String token);
	
}
