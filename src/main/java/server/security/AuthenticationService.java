package server.security;

import server.model.Token;
import server.model.User;

public interface AuthenticationService {

	Token authenticate(String login, String password);

	boolean checkToken(String token);

	void logout(String token);

	User currentUser();
	
}
