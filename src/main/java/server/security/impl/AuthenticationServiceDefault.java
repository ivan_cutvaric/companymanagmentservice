package server.security.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import server.model.User;
import server.model.Token;
import server.security.AuthenticationService;
import server.security.TokenManager;
import server.service.UserService;

public class AuthenticationServiceDefault implements AuthenticationService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ApplicationContext applicationContext;

	private final AuthenticationManager authenticationManager;
	private final TokenManager tokenManager;

	public AuthenticationServiceDefault(AuthenticationManager authenticationManager, TokenManager tokenManager) {
		this.authenticationManager = authenticationManager;
		this.tokenManager = tokenManager;
	}

	@PostConstruct
	public void init() {
		System.out.println(" *** AuthenticationServiceImpl.init with: " + applicationContext);
	}
	
	public Token authenticate(String login, String password) {
		System.out.println(" *** AuthenticationServiceImpl.authenticate");
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(login, password);
		try {
			authentication = authenticationManager.authenticate(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			if (authentication.getPrincipal() != null) {
				UserDetails userContext = (UserDetails) authentication.getPrincipal();
				Token newToken = tokenManager.createNewToken(userContext);
				if (newToken == null) {
					return null;
				}
				return newToken;
			}
		} catch (AuthenticationException e) {
			System.out.println(" *** AuthenticationServiceImpl.authenticate - FAILED: " + e.toString());
		}
		return null;
	}

	public boolean checkToken(String token) {
		System.out.println(" *** AuthenticationServiceImpl.checkToken");

		User user = tokenManager.getUser(token);
		if (user == null) {
			return false;
		}
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

		SecurityContextHolder.getContext().setAuthentication(authentication);

		return true;
	}

	public void logout(String token) {
		tokenManager.removeToken(token);
		SecurityContextHolder.clearContext();
	}

	public User currentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		return userService.getByUsername(userDetails.getUsername());
	}
}