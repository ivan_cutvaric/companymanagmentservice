package server.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import server.model.User;
import server.model.Token;
import server.security.TokenManager;
import server.service.TokenService;

public class DatabaseTokenManager implements TokenManager {
	
	@Autowired
	private TokenService tokenService;
	
	public Token createNewToken(UserDetails userDetails) {
		return tokenService.create(userDetails);
	}

	public User removeToken(String token) {
		return tokenService.deleteByToken(token);
	}

	public User getUser(String token) {
		Token entity = tokenService.getByToken(token);
		if (entity == null) {
			return null;
		}
		return entity.getUser();
	}

}
