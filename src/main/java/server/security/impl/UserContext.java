package server.security.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import server.model.User;
import server.model.UserRole;
import server.service.UserService;

public class UserContext implements UserDetails {

	private static final long serialVersionUID = 8178643868949559175L;
	
	private User user;
	private UserService userService;

	public UserContext(User user, UserService userService) {
		this.user = user;
		this.userService = userService;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		for (UserRole role : user.getUserRoles()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getType()));
		}
		return authorities;
	}

	public String getPassword() {
		return user.getPassword();
	}

	public String getUsername() {
		return user.getUsername();
	}

	public boolean isAccountNonExpired() {
		return userService.isNonExpired(user);
	}

	public boolean isAccountNonLocked() {
		return userService.isNonLocked(user);
	}

	public boolean isCredentialsNonExpired() {
		return userService.areCredentialsNonExpired(user);
	}

	public boolean isEnabled() {
		return userService.isEnabled(user);
	}

	@Override
	public boolean equals(Object o) {
		return this == o
			|| o != null && o instanceof UserContext
			&& Objects.equals(user, ((UserContext) o).user);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(user);
	}

	@Override
	public String toString() {
		return "UserContext{" +
			"user=" + user +
			'}';
	}
}